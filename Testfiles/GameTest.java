/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kashif
 */
public class GameTest {
    
    public GameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDiceValues method, of class Game.
     */
//    @Test
//    public void testGetDiceValues() {
//        System.out.println("getDiceValues");
//        Game instance = null;
//        List<DiceValue> expResult = null;
//        List<DiceValue> result = instance.getDiceValues();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of playRound method, of class Game.
     */
    @Test
    public void testPlayRound() {
        System.out.println("playRound");
        Player player = new Player("a",100);
        
        Dice d1 = new Dice();
        Dice d2 = new Dice();
        Dice d3 = new Dice();
//        DiceValue dv = new DiceValue(Heart);
        DiceValue pick = d1.getValue();
        int bet = 5;
        player.setLimit(0);
        Game game = new Game(d1,d2,d3);
        int winnings = 0;
        int result = 0;
        while (winnings==0){
            System.out.println("Starting Balance : "+player.getBalance());
            winnings = game.playRound(player, pick, bet);
//            System.out.println("Winnings = "+ result);
            if(winnings>0){
                if (winnings == 5 ){
                    System.out.println("Matches = 1");
                    int exp = 105;
                    System.out.println("winnings: "+winnings);
                    System.out.println("expected Balance: "+exp);
                    result = player.getBalance();
                    System.out.println("Balnce: " + player.getBalance());     
                    assertEquals(exp,result);
                }
                else if(winnings == 10 ){
                    System.out.println("Matches = 2");
                    int exp = 110;
                    System.out.println("winnings: "+winnings);
                    System.out.println("expected Balance: "+exp);
                    result = player.getBalance();
                    System.out.println("Balnce: " + player.getBalance());     
                    assertEquals(exp,result);
                }
                else if(winnings == 15 ){
                    System.out.println("Matches = 3");
                    System.out.println("winnings: "+winnings);
                    int exp = 115;
                    System.out.println("expected Balance: "+exp);
                    result = player.getBalance();
                    System.out.println("Balnce: " + player.getBalance());     
                    assertEquals(exp,result);
                }
            }     
        }        
    }
    
}
