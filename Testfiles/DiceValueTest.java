/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kashif
 */
public class DiceValueTest {
    
    public DiceValueTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getRandom method, of class DiceValue.
     */
    @Test
    public void testGetRandom() {
        System.out.println("getRandom");
        
        String [] dv = new String []{"crown","anchor","heart","daimond","club","spade"};
        
        int [] rolls = new int [6];
        boolean found = true;        
        for (int i = 0; i<100; i++){
            int value = DiceValue.getRandom().ordinal();
            rolls[value] ++;
        }
        System.out.println("\n");
        System.out.println("Number of times values rolled");
        boolean exp = true;
        for(int j= 0; j <6 ; j++){
            
            System.out.print( dv[j]+": "+rolls[j]+" ");
            
            if(rolls[j]==0){
                found = false;
                break;
            }
        }
        assertEquals(exp, found);
    }
}
