/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kashif
 */
public class CATest {
    
    
    
    public CATest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
   
    
    
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testMain() throws Exception {
        System.out.println("main");
        String[] args = null;
        Player player = new Player("a",100000);
        player.setLimit(0);
        int bet = 5;
        
        List [] dv = new List[2];
        int count = 0;
        boolean random = false;
        for (int i = 0 ; i <100; i++){
                
            for (int j = 0 ; j <2; j++){
                DiceValue pick = DiceValue.getRandom();
                Dice d1 = new Dice();
                Dice d2 = new Dice();
                Dice d3 = new Dice();
                Game game = new Game(d1, d2, d3);
                List<DiceValue> cdv = game.getDiceValues();
                int winnings = game.playRound(player, pick, bet);
                cdv = game.getDiceValues();
                System.out.printf("Rolled %s, %s, %s\n",cdv.get(0), cdv.get(1), cdv.get(2));
                dv[j]=cdv;
            }
            if (dv[0].equals(dv[1])){
                count++;
            }
            System.out.println("next round\n\n");    
        }
        System.out.println("Dice values rolled were same "+count+ " times.");        
        if (count== 100 ){
            random = false;
        }
        else random = true;
        assertEquals(true,random);
        }
    }
   

